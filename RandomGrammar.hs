module RandomGrammar (
    eqProbLeaf,
    eqProbNode,
    probLeaf,
    probNode,
    evalRandIO,
    MonadRandom,
  ) where

import qualified Control.Monad as M
import Control.Monad.Random

eqProbLeaf :: (MonadRandom r) => [a] -> r a
eqProbLeaf xs = do
  i <- getRandomR (0, length xs - 1)
  return $ xs !! i

eqProbNode :: (MonadRandom r) => [r a] -> r a
eqProbNode = M.join . eqProbLeaf

probLeaf :: (MonadRandom r) => [(a, Rational)] -> r a
probLeaf = fromList

probNode :: (MonadRandom r) => [(r a, Rational)] -> r a
probNode = M.join . fromList

{-
noun = eqProbLeaf ["the dog", "the cat", "the ball"]
verb = eqProbLeaf ["ate", "made", "chased"]
vp = verb +++ (return " ") +++ noun
np = noun +++ (return " ") +++ vp

(+++) :: (Monad m) => m [a] -> m [a] -> m [a]
(+++) = M.liftM2 (++)

main = do
  x <- evalRandIO $ np
  print x
-}

