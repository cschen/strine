#!/usr/bin/env runhaskell

import Control.Monad
import Control.Monad.Random
import Data.Char (toUpper)
import RandomGrammar
import Text.Regex

noun = eqProbLeaf [
  "sheila",
  "bastard",
  "Straya",
  "cunt",
  "mate",
  "mateship",
  "sauce bottle",
  "legend",
  "pisser",
  "shrimp on the barbie",
  "a fair go",
  "roo",
  "dingo",
  "hell",
  "croc",
  "tosser",
  "joke",
  "beer",
  "cold one"]
form_of_address = eqProbLeaf [
  "cunt",
  "sheila",
  "mate",
  "ya pisser",
  "ya cunt",
  "ya dingo fucker",
  "ya fuckin' legend",
  "wanker"]
adverb_slash_adjective = eqProbLeaf [
  "fuckin'",
  "bloody"]
adjective = eqProbLeaf [
  "un-Australian",
  "Aussie",
  "fucked",
  "shithouse",
  "legendary",
  "right",
  "proper",
  "pissed",
  "fair"]
adverb = eqProbNode [adverb_slash_adjective, eqProbLeaf ["bloody well", "damned"]]
subject = eqProbNode [return "it", return "that " +++ noun, noun, return "she", return "you", return "youse", return "that"]
np = eqProbNode [(return "a ") +++ np_unqualified, np_unqualified +++ (return "s")]
  where np_unqualified = adjective +++ (return " ") +++ noun
adjp = probNode [(adjective, 6), (adverb +++ (return " ") +++ adjp, 1)]
sentence = probNode [
  (subject +++ (eqProbLeaf ["'s ", "'ll be ", "'s gonna be "]) +++ eqProbNode [np, adjp], 7),
  (subject +++ (eqProbLeaf ["'s gonna want ", " wants "]) +++ np, 2),
  (adverb_slash_adjective +++ (return " ") +++ noun, 3),
  (return "crikey", 1),
  (return "strewth", 1)]
  +++
  probNode [(return "", 1), (return ", " +++ form_of_address, 2)]
  +++
  probLeaf [(".", 9), ("!", 1)]

cleanSentence =
  sentence >>= (return . cleanString)
cleanParagraph =
  probNode [(cleanSentence, 1), (cleanSentence +++ (return " ") +++ cleanParagraph, 3)]
cleanParagraphs =
  probNode [(cleanParagraph, 1), (cleanParagraph +++ (return "\n") +++ cleanParagraphs, 4)]

cleanString :: String -> String
cleanString = capitalise
	. (rplc "you's" "you're")
	. (rplc "youse's" "youse all")
	. (rplc "youse's" "youse all")
	. (rplc "you wants" "you want")
	where
	rplc a b = flip (subRegex $ mkRegex a) b
	capitalise (c:cs) = (toUpper c):cs

(+++) :: (Monad m) => m [a] -> m [a] -> m [a]
(+++) = liftM2 (++)

main = (evalRandIO cleanParagraphs) >>= putStrLn

